﻿using System.Collections.Generic;
using System.Linq;

namespace Raft
{
    public struct NodeSettings
    {
        public string NodeId { get; set; }
        public int TimeoutInMs { get; set; }
        public string[] Nodes { get; set; }
        public int Majority => ((Nodes?.Length ?? 0) + 3) / 2;
        public IList<string> OtherNodes()
        {
            var tmpThis = this;
            return tmpThis.Nodes.Where(node => node != tmpThis.NodeId).ToList();
        }
    }
}