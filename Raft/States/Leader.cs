﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using log4net.Core;
using Raft.Messages;

namespace Raft.States
{
    internal class Leader<T> : State<T>
    {
        private readonly Dictionary<string, LogReplicationAgent> _states = new Dictionary<string, LogReplicationAgent>();

        public Leader(Node<T> node)
            : base(node)
        {
        }

        internal override void EnterState()
        {
            Node.LeaderId = Node.Id;
            
            foreach (var otherNode in Node.Settings.OtherNodes())
            {
                _states[otherNode] =
                    new LogReplicationAgent(
                        TimeSpan.FromMilliseconds(Node.Settings.TimeoutInMs / 2.0),
                        Node.State.LogEntries.Count,
                        otherNode,
                        Logger);
            }

            BroadcastHeartbeat();
        }

        internal override void ProcessVoteRequest(RequestVote request)
        {
            if (request.Term > CurrentTerm)
            {
                Logger.DebugFormat(
                    "Received a vote request from a node with a higher term ({0}'s term is {1}, our {2}). Stepping down and process vote.",
                    request.CandidateId,
                    request.Term,
                    Node.State.CurrentTerm);

                Node.SwitchToAndProcessMessage(NodeStatus.Follower, request);
                return;
            }

            Logger.TraceFormat(
                "Received a vote request from a node with a lower term, we refuse (Msg : {0})",
                request);
            
            Node.SendVote(request.CandidateId, false);
        }

        internal override void ProcessVote(GrantVote vote)
        {
            Logger.TraceFormat(
                "Received a vote but we are no longer interested: {0}", vote);
        }

        internal override void ProcessAppendEntries(AppendEntries<T> appendEntries)
        {
            if (appendEntries.LeaderTerm >= CurrentTerm)
            {
                Logger.InfoFormat(
                    "Received AppendEntries from a probable leader, stepping down.");

                Node.SwitchToAndProcessMessage(NodeStatus.Follower, appendEntries);
                return;
            }

            Logger.DebugFormat(
                "Received AppendEntries from an invalid leader, refusing ({0}).", appendEntries);
            AppendEntriesAck reply = new AppendEntriesAck(Node.Id, CurrentTerm, false);
            Node.SendMessage(appendEntries.LeaderId, reply);
        }

        internal override void ProcessAppendEntriesAck(AppendEntriesAck appendEntriesAck)
        {
            Logger.TraceFormat("Received AppendEntriesAck ({0}).", appendEntriesAck);

            if (appendEntriesAck.Term > CurrentTerm)
            {
                Logger.DebugFormat("Term is higher, I resign.");
                Node.SwitchTo(NodeStatus.Follower);
                return;
            }

            string followerId = appendEntriesAck.NodeId;
            var followerLogState = _states[followerId];
            followerLogState.ProcessAppendEntriesAck(appendEntriesAck.Success);
            var message = followerLogState.GetAppendEntries(Node.State.LogEntries);
            if (appendEntriesAck.Success)
            {
                UpdateCommitIndex();                
            }

            if (message == null)
            {
                return;
            }

            message.LeaderId = Node.Id;
            message.LeaderTerm = CurrentTerm;
            message.CommitIndex = Node.LastCommit;
            Node.SendMessage(followerId, message);
        }

        protected override void HeartbeatTimeouted()
        {
            BroadcastHeartbeat();
        }

        private void UpdateCommitIndex()
        {
            var ordered = _states.Values.Select(state => state.MinSynchronizedIndex).OrderBy(value => value);
            int index = Node.Settings.Nodes.Length - Node.Settings.Majority + 1;
            int commitIndex = ordered.ElementAt(index);
            Node.Commit(commitIndex);
        }

        private void BroadcastHeartbeat()
        {
            foreach (var entry in _states)
            {
                var followerLogState = entry.Value;
                var message = followerLogState.GetAppendEntries(Node.State.LogEntries);
                if (message == null) continue;
                message.CommitIndex = Node.LastCommit;
                message.LeaderId = Node.Id;
                message.LeaderTerm = CurrentTerm;
                Node.SendMessage(entry.Key, message);
            }

            ResetTimeout(0, .5);
        }

        private class LogReplicationAgent
        {
            private const int MaxBatch = 20;
            private readonly TimeSpan _maxDelay;
            private readonly ILog _logger;
            private int _lastSentIndex = -1;
            private bool _flyingTransaction;
            private DateTime _lastSentMessageTime;

            public LogReplicationAgent(TimeSpan maxDelay, int logSize, string nodeId, ILoggerWrapper master)
            {
                _maxDelay = maxDelay;
                MinSynchronizedIndex = logSize - 1;
                _lastSentMessageTime = DateTime.MinValue;
                _logger = LogManager.GetLogger(
                    $"{master.Logger.Name}(Replicator for {nodeId})");
            }

            public int MinSynchronizedIndex { get; private set; }
            private bool DelayElapsed => (DateTime.Now - _lastSentMessageTime) >= _maxDelay;
            public AppendEntries<T> GetAppendEntries(IList<LogEntry<T>> log)
            {
                if (_flyingTransaction)
                {
                    return null;
                }

                int entriesToSend = Math.Max(
                    0, Math.Min(MaxBatch, log.Count - MinSynchronizedIndex - 1));

                if (entriesToSend == 0 && !DelayElapsed)
                {
                    return null;
                }

                _flyingTransaction = true;
                var message = new AppendEntries<T>
                                  {
                                      PrevLogIndex = MinSynchronizedIndex,
                                      PrevLogTerm =
                                          MinSynchronizedIndex < 0
                                              ? -1
                                              : log[MinSynchronizedIndex].Term,
                                      Entries = new LogEntry<T>[entriesToSend]
                                  };
                int offset = MinSynchronizedIndex + 1;
                
                _logger.TraceFormat(
                    "Replicating {0} entries starting at {1}.", entriesToSend, offset);

                for (int i = 0; i < entriesToSend; i++)
                {
                    message.Entries[i] = log[i + offset];
                }

                _lastSentIndex = offset + entriesToSend - 1;
                _lastSentMessageTime = DateTime.Now;
                return message;
            }

            public void ProcessAppendEntriesAck(bool success)
            {
                _flyingTransaction = false;
                if (success)
                {
                    MinSynchronizedIndex = _lastSentIndex;
                    return;
                }

                MinSynchronizedIndex--;
            }
        }
    }
}
