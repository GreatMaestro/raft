﻿using Raft.Messages;

namespace Raft.States
{
    internal class Follower<T> : State<T>
    {
        public Follower(Node<T> node)
            : base(node)
        {
        }

        internal override void EnterState()
        {
            ResetTimeout(.2);
        }

        internal override void ProcessVoteRequest(RequestVote request)
        {
            bool vote;
            var currentTerm = CurrentTerm;
            if (request.Term < currentTerm)
            {
                Logger.TraceFormat("Vote request from node with lower term. Declined {0}.", request);
                vote = false;
            }
            else
            {
                if (request.Term > currentTerm)
                {
                    Logger.DebugFormat(
                            "Vote request from node with higher term. Updating our term. {0}",
                            request);

                    Node.State.CurrentTerm = request.Term;
                }

                if (Node.State.LogIsBetterOrSameAs(request.LastLogTerm, request.LastLogIndex))
                {
                    vote = false;
                    Logger.TraceFormat(
                            "Vote request from node with less information. We do not vote. Message: {0}.",
                            request);
                }
                else if (string.IsNullOrEmpty(Node.State.VotedFor)
                    || Node.State.VotedFor == request.CandidateId)
                {
                    Logger.TraceFormat(
                            "We do vote for node {1}. Message: {0}.", request, request.CandidateId);
                    vote = true;
                    Node.State.VotedFor = request.CandidateId;
                    
                    ResetTimeout(0, 2);
                }
                else
                {
                    vote = false;
                    Logger.TraceFormat(
                            "We already voted. We do not grant vote. Message: {0}.", request);
                }
            }
            
            Node.SendVote(request.CandidateId, vote);
        }

        internal override void ProcessVote(GrantVote vote)
        {
            Logger.DebugFormat(
                    "Received a vote but I am a follower. Message discarded: {0}.", vote);
        }

        internal override void ProcessAppendEntries(AppendEntries<T> appendEntries)
        {
            bool result;
            if (appendEntries.LeaderTerm < CurrentTerm)
            {
                Logger.DebugFormat(
                    "Reject an AppendEntries from an invalid leader ({0}).", appendEntries);
                result = false;
            }
            else
            {
                Node.LeaderId = appendEntries.LeaderId;
                if (appendEntries.LeaderTerm > CurrentTerm)
                {
                    Logger.TraceFormat("Upgrade our term to {0}.", CurrentTerm);
                    Node.State.CurrentTerm = appendEntries.LeaderTerm;
                }

                if (Node.State.EntryMatches(
                    appendEntries.PrevLogIndex, appendEntries.PrevLogTerm))
                {
                    Logger.TraceFormat(
                        "Process an AppendEntries request: {0}", appendEntries);
                    Node.State.AppendEntries(appendEntries.PrevLogIndex, appendEntries.Entries);
                    Node.Commit(appendEntries.CommitIndex);
                    result = true;
                }
                else
                {
                    Logger.DebugFormat(
                            "Reject an AppendEntries that does not match our log ({0}).",
                            appendEntries);
                    result = false;
                }
            }

            var reply = new AppendEntriesAck(Node.Id, CurrentTerm, result);
            Node.SendMessage(appendEntries.LeaderId, reply);
            ResetTimeout(.2);
        }

        protected override void HeartbeatTimeouted()
        {
            if (Done)
            {
                return;
            }

            Logger.Info(
                    "Timeout elapsed without sign from current leader. Trigger an election.");

            Node.SwitchTo(NodeStatus.Candidate);
        }
    }
}