﻿using System;
using System.Threading;
using log4net;
using Raft.Messages;

namespace Raft.States
{
    internal abstract class State<T>
    {
        protected readonly Node<T> Node;

        private static readonly Random Seed = new Random();

        protected State(Node<T> node)
        {
            Node = node;
        }

        protected bool Done { get; private set; }
        protected ILog Logger => Node.Logger;
        protected long CurrentTerm => Node.State.CurrentTerm;
        private NodeSettings Settings => Node.Settings;
        private Timer HeartBeatTimer { get; set; }

        internal abstract void EnterState();
        internal abstract void ProcessVoteRequest(RequestVote request);
        internal abstract void ProcessVote(GrantVote vote);
        internal abstract void ProcessAppendEntries(AppendEntries<T> appendEntries);
        internal void ExitState()
        {
            HeartBeatTimer?.Dispose();
            Done = true;
        }

        internal virtual void ProcessAppendEntriesAck(AppendEntriesAck appendEntriesAck)
        {
            Logger.WarnFormat(
                "Received ProcessAppendEntriesAck but I am not a leader, discarded: {0}",
                appendEntriesAck);
        }

        protected void ResetTimeout(double randomPart = 0.0, double fixPart = 1.0)
        {
            if (Done)
            {
                return;
            }

            HeartBeatTimer?.Dispose();

            int timeout;
            if (Settings.TimeoutInMs != Timeout.Infinite)
            {
                timeout =
                    (int)
                    (((Seed.NextDouble() * randomPart * 2.0) + (fixPart - randomPart))
                     * Settings.TimeoutInMs);
                if (timeout < 10)
                {
                    timeout = 10;
                }

                Logger.DebugFormat("Set timeout to {0} ms.", timeout);
            }
            else
            {
                timeout = Settings.TimeoutInMs;
                if (Logger.IsDebugEnabled)
                {
                    Logger.Debug("Set timeout to infinite.");
                }
            }

            HeartBeatTimer = new Timer(
                InternalTimerHandler, null, timeout, Timeout.Infinite);
        }

        protected abstract void HeartbeatTimeouted();

        private void InternalTimerHandler(object state)
        {
            Node.Sequence(HeartbeatTimeouted);
        }
    }
}