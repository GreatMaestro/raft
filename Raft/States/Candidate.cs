﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raft.Messages;

namespace Raft.States
{
    internal class Candidate<T> : State<T>
    {
        private readonly IDictionary<string, GrantVote> _voteReceived;

        public Candidate(Node<T> node)
            : base(node)
        {
            _voteReceived = new Dictionary<string, GrantVote>();
        }

        internal override void EnterState()
        {
            _voteReceived.Clear();
            var nextTerm = Node.IncrementTerm();

            Node.SendVote(Node.Id, true);
            RegisterVote(new GrantVote(true, Node.Id, nextTerm));
            
            Logger.TraceFormat("Broadcast a vote request for term {0}", CurrentTerm);
            var request = new RequestVote(nextTerm, Node.Id, Node.State.LastPersistedIndex, Node.State.LastPersistedTerm);
            Node.SendToOthers(request);
            ResetTimeout(.3);
        }

        internal override void ProcessVoteRequest(RequestVote request)
        {
            var currentTerm = CurrentTerm;
            if (request.CandidateId == Node.Id)
            {
                return;
            }

            if (request.Term > currentTerm)
            {
                Logger.DebugFormat(
                    "Received vote request from node with higher term ({0}'s term is {1}, our {2}). Resigning.",
                    request.CandidateId,
                    request.Term,
                    currentTerm);

                Node.SwitchToAndProcessMessage(NodeStatus.Follower, request);
                return;
            }

            if (request.Term == CurrentTerm
                    && !Node.State.LogIsBetterOrSameAs(request.LastLogTerm, request.LastLogIndex))
            {
                Logger.DebugFormat(
                    "Received vote request from node with better log ({0}'s log is at {1}, our at {2}). Resigning.",
                    request.LastLogIndex,
                    Node.LastCommit,
                    currentTerm);

                Node.SwitchToAndProcessMessage(NodeStatus.Follower, request);
                return;
            }

            Logger.TraceFormat(
                "Received a vote request from a node with a lower term. We decline {0}",
                request);
            Node.SendVote(request.CandidateId, false);
        }

        internal override void ProcessVote(GrantVote vote)
        {
            if (vote.VoterTerm > CurrentTerm)
            {
                Node.State.CurrentTerm = vote.VoterTerm;
                Logger.DebugFormat(
                    "Received a vote from a node with a higher term. Dropping candidate status down. Message discarded {0}.",
                    vote);
                Node.SwitchTo(NodeStatus.Follower);
                return;
            }

            if (_voteReceived.ContainsKey(vote.VoterId))
            {
                // голос от этого узла уже был получен
                Logger.WarnFormat(
                    "We received a second vote from {0}. Initial vote: {1}. Second vote: {2}.",
                    vote.VoterId,
                    _voteReceived[vote.VoterId],
                    vote);
                return;
            }

            RegisterVote(vote);
        }

        internal override void ProcessAppendEntries(AppendEntries<T> appendEntries)
        {
            if (appendEntries.LeaderTerm >= CurrentTerm)
            {
                Logger.InfoFormat(
                    "Received AppendEntries from a probable leader, stepping down ({0}).",
                    appendEntries);
                Node.SwitchToAndProcessMessage(NodeStatus.Follower, appendEntries);
            }
            else
            {
                Logger.Debug("Received AppendEntries from an invalid leader, refusing.");
                var reply = new AppendEntriesAck(Node.Id, CurrentTerm, false);
                Node.SendMessage(appendEntries.LeaderId, reply);
            }
        }

        protected override void HeartbeatTimeouted()
        {
            if (_voteReceived.Count == Node.Settings.Nodes.Length && Logger.IsDebugEnabled)
            {
                Logger.DebugFormat("We got all votes back, but I am not elected.");
            }

            // ни выборов, ни лидера, начинаем новое голосование
            EnterState();
        }

        private void RegisterVote(GrantVote vote)
        {
            _voteReceived.Add(vote.VoterId, vote);

            var votes = _voteReceived.Values.Count(grantVote => grantVote.VoteGranted);

            if (votes < Node.Settings.Majority)
            {
                return;
            }

            var nodes = new StringBuilder();
            foreach (var key in _voteReceived.Keys)
            {
                nodes.AppendFormat("{0},", key);
            }
            
            // победа
            Logger.InfoFormat("I am the LEADER (with {0} votes): {1}.", _voteReceived.Count, nodes);
            Node.SwitchTo(NodeStatus.Leader);
        }
    }
}