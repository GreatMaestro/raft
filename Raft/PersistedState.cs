﻿using System.Collections.Generic;

namespace Raft
{
    public class PersistedState<T>
    {
        private long _currentTerm;

        public PersistedState()
        {
            LogEntries = new List<LogEntry<T>>();
        }

        public long CurrentTerm
        {
            get => _currentTerm;

            set
            {
                if (_currentTerm == value)
                {
                    return;
                }

                _currentTerm = value;
                VotedFor = null;
            }
        }
        public long LastPersistedTerm => LogEntries.Count > 0
            ? LogEntries[LogEntries.Count - 1].Term
            : 0;
        public int LastPersistedIndex => LogEntries.Count > 0 ? LogEntries.Count - 1 : -1;
        public string VotedFor { get; set; }
        public IList<LogEntry<T>> LogEntries { get; }

        public void AddEntry(LogEntry<T> logEntry)
        {
            if (logEntry.Term == 0)
            {
                var newEntry = new LogEntry<T>(logEntry.Command, CurrentTerm);
                LogEntries.Add(newEntry);
            }
            else
            {
                LogEntries.Add(logEntry);
            }
        }

        public void AddEntry(T command)
        {
            var newEntry = new LogEntry<T>(command, _currentTerm);
            LogEntries.Add(newEntry);
        }

        public bool LogIsBetterOrSameAs(long lastLogTerm, long lastLogIndex)
        {
            if (LogEntries.Count == 0)
            {
                return false;
            }

            var lastEntryId = LogEntries.Count - 1;
            var lastEntry = LogEntries[lastEntryId];
            if (lastEntry.Term > lastLogTerm)
            {
                return true;
            }

            if (lastEntry.Term < lastLogTerm)
            {
                return false;
            }

            return lastEntryId > lastLogIndex;
        }

        public bool EntryMatches(int index, long term)
        {
            if (index >= LogEntries.Count || index < 0)
            {
                return index == -1;
            }

            return LogEntries[index].Term == term;
        }

        public void AppendEntries(int prevLogIndex, IEnumerable<LogEntry<T>> entries)
        {
            if (entries == null)
            {
                return;
            }

            prevLogIndex++;
            foreach (var logEntry in entries)
            {
                if (prevLogIndex == LogEntries.Count)
                {
                    LogEntries.Add(logEntry);
                }
                else
                {
                    LogEntries[prevLogIndex] = logEntry;
                }

                prevLogIndex++;
            }
        }
    }
}