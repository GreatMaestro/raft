﻿namespace Raft
{
    public sealed class LogEntry<T>
    {
        public LogEntry(T command, long term)
        {
            Command = command;
            Term = term;
        }

        public LogEntry(T command)
        {
            Command = command;
        }

        public T Command { get; }
        public long Term { get; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((LogEntry<T>)obj);
        }

        public override int GetHashCode()
        {
            return Term.GetHashCode();
        }

        private bool Equals(LogEntry<T> other)
        {
            return Term == other.Term;
        }
    }
}