﻿namespace Raft
{
    public enum NodeStatus
    {
        Initializing,
        Follower,
        Candidate,
        Leader
    }
}