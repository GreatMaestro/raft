﻿using System;
using log4net;
using Michonne.Interfaces;
using Raft.Commands;
using Raft.Messages;
using Raft.Services;
using Raft.States;

namespace Raft
{
    public sealed class Node<T> : IDisposable
    {
        private readonly ISequencer _sequencer;
        
        private readonly ILog _logger;

        private readonly IMiddleware _internalMiddleware;

        private readonly IStateMachine<T> _stateMachine;

        private NodeSettings _settings;

        private State<T> _currentState;

        private int _lastCommit = -1;

        public Node(ISequencer sequencer, NodeSettings settings, IMiddleware middleware, IStateMachine<T> machine)
        {
            Id = settings.NodeId;
            _settings = settings;
            _internalMiddleware = middleware;
            _stateMachine = machine;
            _sequencer = sequencer;
            Status = NodeStatus.Initializing;
            State = new PersistedState<T>();
            _logger = LogManager.GetLogger($"Node[{Id}]");
        }

        public NodeStatus Status { get; private set; }
        public string Id { get; }
        public int MessagesCount { get; private set; }
        public PersistedState<T> State { get; }

        public int LastCommit => _lastCommit;

        internal ILog Logger => _logger;
        internal NodeSettings Settings => _settings;
        internal string LeaderId { get; set; }

        public void Initialize()
        {
            if (_internalMiddleware == null)
            {
                throw new InvalidOperationException("Must call SetInternalMiddleware first!");
            }

            if (Status != NodeStatus.Initializing)
            {
                throw new InvalidOperationException("Node is already initialized.");
            }

            _logger.InfoFormat("Middleware registration to address {0}.", Id);
            _internalMiddleware.RegisterEndPoint(Id, MessageReceived);
            
            SwitchTo(
                _settings.OtherNodes().Count == 0 ? NodeStatus.Leader : NodeStatus.Follower);
        }

        public void AddEntry(T command)
        {
            _sequencer.Dispatch(() => State.AddEntry(command));
        }

        public void Dispose()
        {
            if (_logger.IsInfoEnabled)
            {
                _logger.Info("Stopping Node.");
            }

            if (_currentState == null)
            {
                return;
            }
            _currentState.ExitState();
            _currentState = null;
        }

        public bool SendCommand(T command)
        {
            if (String.IsNullOrEmpty(LeaderId))
            {
                // нет лидера
                return false;
            }

            Sequence(
                () =>
                {
                    if (String.IsNullOrEmpty(LeaderId))
                    {
                        // лидер исчез
                        return;
                    }

                    var message = new SendCommand<T>(command);
                    if (LeaderId == Id)
                    {
                        SequencedMessageReceived(message);
                    }
                    else
                    {
                        SendMessage(LeaderId, message);
                    }
                });
            return true;
        }

        internal void SendVote(string candidate, bool voteGranted)
        {
            if (voteGranted)
            {
                State.VotedFor = candidate;
                LeaderId = string.Empty;

                if (candidate == Id)
                {
                    return;
                }
            }

            SendMessage(candidate, new GrantVote(voteGranted, Id, State.CurrentTerm));
        }

        internal void SwitchTo(NodeStatus status)
        {
            SequencedSwitch(status);
        }

        internal void SwitchToAndProcessMessage(NodeStatus status, object message)
        {
            SequencedSwitch(status);
            SequencedMessageReceived(message);
        }

        internal long IncrementTerm()
        {
            var nextTerm = State.CurrentTerm + 1;
            State.CurrentTerm = nextTerm;
            return nextTerm;
        }

        internal void Commit(int newCommit)
        {
            if (_lastCommit >= newCommit)
            {
                return;
            }

            for (var i = _lastCommit + 1; i <= newCommit; i++)
            {
                _stateMachine.Commit(State.LogEntries[i].Command);
                Logger.TraceFormat("Committed index is {0}.", i);
            }

            _lastCommit = newCommit;
            Logger.DebugFormat("Commit index is now {0}.", _lastCommit);
        }

        internal void SendMessage(string dest, object message)
        {
            _internalMiddleware.SendMessage(dest, message);
        }

        internal void SendToOthers(object message)
        {
            // отправка запроса всем узлам
            if (_logger.IsTraceEnabled())
            {
                _logger.TraceFormat("Broadcast message to all other nodes: {0}", message);
            }

            foreach (var otherNode in _settings.OtherNodes())
            {
                _internalMiddleware.SendMessage(otherNode, message);
            }
        }

        internal void Sequence(Action action)
        {
            _sequencer.Dispatch(action);
        }

        private void SequencedSwitch(NodeStatus status)
        {
            State<T> newState;
            _logger.DebugFormat("Switching status from {0} to {1}", Status, status);
            _currentState?.ExitState();

            switch (status)
            {
                case NodeStatus.Follower:
                    newState = new Follower<T>(this);
                    break;
                case NodeStatus.Candidate:
                    newState = new Candidate<T>(this);
                    break;
                case NodeStatus.Leader:
                    newState = new Leader<T>(this);
                    break;
                default:
                    throw new NotSupportedException("Status not supported");
            }

            Status = status;
            _currentState = newState;
            _currentState.EnterState();
        }

        private void MessageReceived(object obj)
        {
            _sequencer.Dispatch(() => SequencedMessageReceived(obj));
        }

        private void SequencedMessageReceived(object obj)
        {
            if (_currentState == null)
            {
                Logger.DebugFormat("Node is not active, discarding message ({0}).", obj);
                return;
            }

            MessagesCount++;

            var requestVote = obj as RequestVote;
            if (requestVote != null)
            {
                _currentState.ProcessVoteRequest(requestVote);
                return;
            }

            var vote = obj as GrantVote;
            if (vote != null)
            {
                _currentState.ProcessVote(vote);
                return;
            }

            var appendEntries = obj as AppendEntries<T>;
            if (appendEntries != null)
            {
                _currentState.ProcessAppendEntries(appendEntries);
                return;
            }

            var appendEntriesAck = obj as AppendEntriesAck;
            if (appendEntriesAck != null)
            {
                _currentState.ProcessAppendEntriesAck(appendEntriesAck);
                return;
            }

            _logger.ErrorFormat("Message not recognized (Type: {0}, Message: {1})", obj.GetType(), obj);
        }
    }
}
