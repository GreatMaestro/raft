﻿using System;

namespace Raft.Services
{
    public interface IMiddleware
    {
        bool SendMessage(string addressDest, object message);
        void RegisterEndPoint(string address, Action<object> messageReceived);
    }
}