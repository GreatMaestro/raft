﻿namespace Raft.Services
{
    public interface IStateMachine<in T>
    {
        void Commit(T command);
    }
}