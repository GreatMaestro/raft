﻿using System.Reflection;
using log4net;
using log4net.Core;

namespace Raft
{
    public static class LogExtensions
    {
         public static void TraceFormat(
             this ILog logger, string format, params object[] formatItems)
         {
             logger.Logger.Log(MethodBase.GetCurrentMethod().DeclaringType, Level.Trace, string.Format(format, formatItems), null);
         }

        public static bool IsTraceEnabled(this ILog logger)
        {
            return logger.Logger.IsEnabledFor(Level.Trace);
        }
    }
}