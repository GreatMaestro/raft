﻿namespace Raft.Commands
{
    public class SendCommand<T>
    {
        private readonly T _command;

        private string _id;

        private long _identifier;

        public SendCommand(T command)
        {
            _command = command;
        }

        public long Identifier => _identifier;
        public string Id => _id;
        public T Command => _command;
    }   
}