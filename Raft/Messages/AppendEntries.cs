﻿namespace Raft.Messages
{
    public class AppendEntries<T>
    {
        public long LeaderTerm { get; set; }
        public string LeaderId { get; set; }
        public int PrevLogIndex { get; set; }
        public long PrevLogTerm { get; set; }
        public LogEntry<T>[] Entries { get; set; }
        public int CommitIndex { get; set; }

        public override string ToString()
        {
            return
                string.Format(
                    "AppendEntries: LeaderId: {1} (Term {0}), PrevLog Index and Term: {2}, {3}, Commit {4}, Entries: {5}",
                    LeaderTerm,
                    LeaderId,
                    PrevLogIndex,
                    PrevLogTerm,
                    CommitIndex,
                    Entries?.Length ?? 0);
        }
    }
}