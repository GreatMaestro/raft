﻿namespace Raft.Messages
{
    public class RequestVote
    {
        public RequestVote(long term, string candidateId, long lastLogIndex, long lastLogTerm)
        {
            Term = term;
            CandidateId = candidateId;
            LastLogIndex = lastLogIndex;
            LastLogTerm = lastLogTerm;
        }

        public long Term { get; }
        public string CandidateId { get; }
        public long LastLogIndex { get; }
        public long LastLogTerm { get; }

        public override string ToString()
        {
            return string.Format(
                "RequestVote: Candidate: {1} (Term {0}, LastLogEntry Index and Term: {2}, {3})",
                Term,
                CandidateId,
                LastLogIndex,
                LastLogTerm);
        }
    }
}