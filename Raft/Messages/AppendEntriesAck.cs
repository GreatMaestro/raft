﻿namespace Raft.Messages
{
    public class AppendEntriesAck
    {
        public AppendEntriesAck(string nodeId, long term, bool success)
        {
            Term = term;
            Success = success;
            NodeId = nodeId;
        }

        public string NodeId { get; }
        public long Term { get; set; }
        public bool Success { get; set; }
        
        public override string ToString()
        {
            return $"AppendEntriesAck: NodeId: {NodeId}, Term: {Term}, Success: {Success}";
        }
    }
}