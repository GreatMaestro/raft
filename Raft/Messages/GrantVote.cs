﻿namespace Raft.Messages
{
    public sealed class GrantVote
    {
        public GrantVote(bool voteGranted, string voterId, long voterTerm)
        {
            VoteGranted = voteGranted;
            VoterId = voterId;
            VoterTerm = voterTerm;
        }

        public bool VoteGranted { get; }
        public string VoterId { get; }
        public long VoterTerm { get; }

        public override string ToString()
        {
            return string.Format(
                "GrantVote: Voter {1} {0} (VoterTerm: {2})",
                VoteGranted ? "voted" : "did not vote",
                VoterId,
                VoterTerm);
        }
    }
}